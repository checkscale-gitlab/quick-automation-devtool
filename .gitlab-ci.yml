variables:
  PACKAGE_NAME: qad
  CARGO_HOME: "$CI_PROJECT_DIR/.cargo"
  PACKAGE_REGISTRY_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${PACKAGE_NAME}/${CI_COMMIT_TAG}/${PACKAGE_NAME}"

stages:
  - build
  - test
  - publish
  - release

cache: &global_cache
  key: one-key-to-rule-them-all
  paths:
    - .cargo/
    - target/
  policy: pull

build:
  image: rust:latest
  stage: build
  script:
    - cargo build --release
    - test "$CI_COMMIT_TAG" == "$(target/release/${PACKAGE_NAME} --version)"
    - mkdir bin
    - cp target/release/${PACKAGE_NAME} bin/${PACKAGE_NAME}
  artifacts:
    paths:
      - bin
  rules:
    - if: $CI_COMMIT_TAG
  cache:
    <<: *global_cache
    policy: pull-push

build-deb:
  stage: build
  image: rust:latest
  needs:
    - build
  rules:
    - if: $CI_COMMIT_TAG
  before_script:
    - cargo install cargo-deb
    - rustup target add x86_64-unknown-linux-musl
  script:
    - bin/qad autocomplete > /tmp/qad.bash
    - cargo deb --target x86_64-unknown-linux-musl --output target/release/${PACKAGE_NAME}.deb
  artifacts:
    paths:
      - target/release/${PACKAGE_NAME}.deb

clippy:
  image: rustlang/rust:nightly
  script:
    - cargo clippy --tests --color=always

lint-commits:
  image: node
  cache: []
  rules:
    - if: '$CI_MERGE_REQUEST_TARGET_BRANCH_NAME'
  script:
    - npm install -g commitlint
    - commitlint --from=$CI_MERGE_REQUEST_DIFF_BASE_SHA --to=$CI_COMMIT_SHORT_SHA

fmt:
  image: rustlang/rust:nightly
  script:
    - cargo fmt --check

rust-nightly:
  image: rustlang/rust:nightly
  script:
    - cargo test -- --color=always

shellcheck:
  image: koalaman/shellcheck-alpine:stable
  script: find -name '*.sh' -not -path '*/.*' -not -path '*/target/*' -type f -exec shellcheck {} '+'

test-autocomplete:
  image: bash
  script:
    - tools/bash-unit-tests.sh

upload binary:
  stage: publish
  needs:
    - build
    - build-deb
  image: curlimages/curl:latest
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - 'curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file bin/${PACKAGE_NAME} ${PACKAGE_REGISTRY_URL}'
    - 'curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file target/release/${PACKAGE_NAME}.deb ${PACKAGE_REGISTRY_URL}.deb'

changelog:
  stage: publish
  image: alpine
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - apk add bash git
    - printf 'CHANGELOG=%s\n' "$(./tools/changelog --format web)" > description.env
  artifacts:
    reports:
      dotenv: description.env

release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  needs:
    - upload binary
    - changelog
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - echo "releasing ${PACKAGE_NAME}..."
    - echo $CHANGELOG
  release:
    name: 'Release $CI_COMMIT_TAG'
    description: '$CHANGELOG'
    tag_name: '$CI_COMMIT_TAG'
    ref: '$CI_COMMIT_TAG'
    assets:
      links:
        - name: '${PACKAGE_NAME}-${CI_COMMIT_TAG}'
          url: "${PACKAGE_REGISTRY_URL}"
          link_type: other
        - name: '${PACKAGE_NAME}-${CI_COMMIT_TAG}.deb'
          url: "${PACKAGE_REGISTRY_URL}.deb"
          link_type: package
